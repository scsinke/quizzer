interface Cookie {
  name: string;
  value: string;
}

const isLoggedIn = (): boolean => {
  const cookie = getCookie("connect.sid")
  if (cookie) return true
  return false;
}

const getCookie = (cookieName: string): Cookie | undefined => {
  const name = `${cookieName}=`
  // Decode cookie string to support special characters
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(";");

  const cookie = ca.find(cookie => cookie.includes(name))
  if (cookie) {
    return { name, value: cookie.substring(name.length) }
  }
}

export default isLoggedIn;
