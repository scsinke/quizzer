import React, { useEffect, useState } from 'react';
import { message, Card, Spin, List } from 'antd';
import { TeamScore } from '@quizzer/common';
import { Component } from 'react';

const { Meta } = Card;

const fetchTeamScore = async (quizId: string) => {
  try {
    const res = await fetch(`/api/v1/quiz/${quizId}/score`, {
      credentials: 'same-origin',
      cache: "no-cache",
    });

    const body = await res.json()

    if (res.ok) {
      return body
    } else {
      throw Error(body.message)
    }
  } catch (error) {
    console.error(error)
  }
}

interface IProps {
  quizId: string;
}

interface IState {
  scores: TeamScore[];
}

class TeamScoreComponent extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      scores: []
    }
  }

  async componentDidMount() {
    try {
      const scores: TeamScore[] = await fetchTeamScore(this.props.quizId)
      scores.sort((a, b) => b.points - a.points)
      this.setState({
        scores
      });
    } catch (error) {
      message.error(error)
    }
  }


  card(index: number, color: string) {
    const score = this.state.scores[index]
    if (!score) { return }
    return (
      <Card
        cover={
          <div
            style={{ backgroundColor: color, textAlign: "center" }}
          >
            {index + 1}
          </div>
        }
      >
        <Meta
          title={score.name}
          description={score.points}
        />
      </Card>
    )
  }

  topTeams() {
    return (
      <div>
        {this.card(0, "goldenrod")}
        {this.card(1, "silver")}
        {this.card(2, "sienna")}
      </div>
    )
  }

  rest() {
    const data = this.state.scores.slice(3, this.state.scores.length);
    if (!data.length) { return }
    return (
      <List
        dataSource={this.state.scores.slice(3, this.state.scores.length)}
        renderItem={score => (
          <List.Item>
            {score.name}  / {score.points}
          </List.Item>
        )}
      />
    )
  }

  render() {

    if (!this.state.scores.length) {
      return (
        <div>
          No scores yet
          <Spin />
        </div>
      )
    }

    return (
      <div>
        {this.topTeams()}
        {this.rest()}
      </div>
    )
  }
}

export default TeamScoreComponent;
