import { FormikHelpers } from "formik";
import { message } from "antd";
import UserForm from "./UserForm";
import { User, UserSchema } from "@quizzer/common";
import React from "react";
import { useHistory } from "react-router-dom";

const register = async (schema: User) => {
  const res = await fetch('/api/v1/register', {
    method: 'POST',
    credentials: 'same-origin',
    cache: "no-cache",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(schema),
  });

  if (!res.ok) {
    const body = await res.json()
    message.error(body.message)
  }
}

const Register = () => {
  const history = useHistory();
  const submit = async (schema: User, { setSubmitting }: FormikHelpers<User>) => {
    await register(schema)
    history.push('/login')
  }

  return (
    <div>
      <h1>Register</h1>
      {UserForm(UserSchema, submit)}
    </div>
  )
}

export default Register;
