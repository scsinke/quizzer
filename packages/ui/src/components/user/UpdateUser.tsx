import { FormikHelpers } from "formik";
import { message } from "antd";
import UserForm from "./UserForm";
import { UserPartial, UserUpdateSchema } from "@quizzer/common";
import React from "react";

const submit = async <T extends UserPartial>(schema: T, { setSubmitting }: FormikHelpers<T>) => {
  const res = await fetch('/api/v1/me', {
    method: 'PUT',
    credentials: 'same-origin',
    cache: "no-cache",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(schema),
  });
  
  if (!res.ok) {
    const body = await res.json()
    message.error(body.message)
  }

  setSubmitting(false);
}

const UpdateUser = () => {
  return(
    <div>
      <h1>User profile</h1>
      {UserForm(UserUpdateSchema, submit, {})}
    </div>
  )
}

export default UpdateUser;
