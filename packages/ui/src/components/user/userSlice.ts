import { createSlice } from "@reduxjs/toolkit";
import IsLoggedIn from "../../functions/isLoggedIn";

export interface UserState {
  isLoggedIn: boolean;
}

const initialState: UserState = {
  isLoggedIn: IsLoggedIn()
}

const user = createSlice({
  name: "user",
  initialState: initialState,
  reducers: {
    isLoggedIn: (state) => {
      state.isLoggedIn = IsLoggedIn();
    }
  },
})

export const { isLoggedIn } = user.actions

export default user.reducer;
