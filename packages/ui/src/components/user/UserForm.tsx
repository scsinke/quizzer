import React from "react";
import { Formik, FormikHelpers } from "formik";
import { Form, Input, SubmitButton, ResetButton } from "formik-antd";
import { ObjectSchema } from "yup";
import { User, UserPartial } from "@quizzer/common";

const UserForm = <T extends User | UserPartial>(
  validationSchema: ObjectSchema<T>,
  submit: { (schema: T, helpers: FormikHelpers<T>): Promise<void> },
  initialValues: T = { name: "", password: "" } as T
) => {

  return (
    <Formik<T>
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={submit}
    >
      <Form>
        <Form.Item name='name' label='Username'>
          <Input name='name' />
        </Form.Item>
        <Form.Item name='password' label='Password'>
          <Input name='password' type='password' />
        </Form.Item>
        <SubmitButton>Submit</SubmitButton>
        <ResetButton>Reset</ResetButton>
      </Form>
    </Formik>
  )
}

export default UserForm;
