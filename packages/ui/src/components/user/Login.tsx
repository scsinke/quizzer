import { FormikHelpers } from "formik";
import { message } from "antd";
import React from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { isLoggedIn } from "./userSlice";
import UserForm from "./UserForm";
import { User, UserSchema } from "@quizzer/common";

const fetchLogin = async (schema: User) => {
  const res = await fetch('/api/v1/login', {
    method: 'POST',
    credentials: 'same-origin',
    cache: "no-cache",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(schema),
  });
  
  if (!res.ok) {
    const body = await res.json()
    message.error(body.message)
  }
}

const Login = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const submit = async (schema: User, { setSubmitting }: FormikHelpers<User>) => {
    await fetchLogin(schema);
    setSubmitting(false)
    dispatch(isLoggedIn())
    history.push('/')
  }

  return(
    <div>
      <h1>Login</h1>
      {UserForm(UserSchema, submit)}
    </div>
  )
}

export default Login;
