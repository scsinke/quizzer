import { message, Button } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { isLoggedIn } from "./userSlice";
import { useHistory } from "react-router-dom";

const fetchLogout = async () => {
  const res = await fetch('/api/v1/logout', {
    method: 'POST',
    credentials: 'same-origin',
    cache: "no-cache",
  });

  if (!res.ok) {
    const body = await res.json();
    message.error(body.message);
  }

  message.success("logged out");
}

const Logout = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const submit = async () => {
    await fetchLogout()
    dispatch(isLoggedIn())
    history.push('/');
  }

  return <Button onClick={submit}>Logout</Button>
}
  export default Logout;
