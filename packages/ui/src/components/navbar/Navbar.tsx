import React from "react";
import { Menu, Avatar, Button } from "antd";
import { useHistory } from "react-router-dom";
import Logout from "../user/Logout";
import { UserState } from "../user/userSlice";

const Navbar = (props: UserState) => {
  const history = useHistory();

  const handleClick = () => {
    history.push("/");
  }

  return (
    <nav className="menuBar">
      <div>
        <Menu mode="horizontal">
          <Menu.Item>
            <Button onClick={handleClick}>
              Quizzer
            </Button>
          </Menu.Item>
        </Menu>
      </div>
      <div>
        {props.isLoggedIn ? <LoggedIn /> : <NotLoggedIn />}
      </div>
    </nav>
  )
}

const NotLoggedIn = () => {
  const history = useHistory();

  const handleClick = (url: string) => {
    history.push(url);
  }

  return (
    <div>
      <Menu mode="horizontal">
        <Menu.Item>
          <Button onClick={() => handleClick('/login')}>
            SignIn
          </Button>
        </Menu.Item>
        <Menu.Item>
          <Button onClick={() => handleClick('/register')}>
            Register
          </Button>
        </Menu.Item>
      </Menu>
    </div>
  )
}

const LoggedIn = () => {
  const history = useHistory();

  const handleClick = () => {
    history.push('/me');
  }

  return (
    <Menu
      mode="horizontal"
    >
      <Menu.Item>
        <div onClick={handleClick}>
          <Avatar>U</Avatar>
        </div>
      </Menu.Item>
      <Menu.Item>
        <Logout />
      </Menu.Item>
    </Menu>
  )
}

export default Navbar;
