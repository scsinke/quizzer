import { object, string, ObjectSchema } from "yup";

export type AnswerQuestion = {
  answer: string;
}

const AnswerQuestionSchema: ObjectSchema<AnswerQuestion> = object({
  answer: string().required(),
}).defined();

export default AnswerQuestionSchema;
