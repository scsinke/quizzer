import { object, string, ObjectSchema } from 'yup';
import { languages } from '../models/question';

export type QuizSchema = {
  name: string;
  language?: string;
  password?: string;
}

const QuizSchema: ObjectSchema<QuizSchema> = object({
  name: string()
    .min(4)
    .required(),
  language: string()
    .oneOf(languages)
    .notRequired(),
  password: string()
    .min(8)
    .notRequired(),
}).defined();

export default QuizSchema;
