import {object, boolean, ObjectSchema} from "yup";

export type UpdateTeamStatus = {
  isAccepted: boolean;
}

const UpdateTeamStatusSchema: ObjectSchema<UpdateTeamStatus> = object({
  isAccepted: boolean().required()
}).defined()

export default UpdateTeamStatusSchema;
