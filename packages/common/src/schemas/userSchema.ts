import {object, string, ObjectSchema } from "yup";
import Quiz from "../models/quiz";

type Partial<T> = {
  [P in keyof T]?: T[P];
}

export type User = {
  _id?: string;
  name: string;
  password?: string;
  quizzes?: Array<{
    quiz: Quiz;
    isOwner: boolean;
    isAccepted?: boolean;
  }>;
}

export type UserQuizes = User['quizzes']

export type UserPartial = Partial<User>;

export const UserSchema: ObjectSchema<User> = object({
  name: string().required(),
  password: string().min(10).required(),
}).defined();

export const UserUpdateSchema: ObjectSchema<UserPartial> = object({
  name: string()
    .notRequired(),
  password: string()
    .min(10)
    .notRequired(),
}).defined()
