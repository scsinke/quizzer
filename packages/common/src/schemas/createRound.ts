import { object, array, string, ObjectSchema } from "yup";
import { categories } from "../models/question";
import { categorieLimit } from "../models/quiz";

export type CreateRound = {
  categories: string[];
}

const CreateRoundSchema: ObjectSchema<CreateRound> = object({
  categories: array().of(
    string()
      .oneOf(categories)
      .required()
  )
    .min(categorieLimit)
    .max(categorieLimit)
    .required()
}).defined()

export default CreateRoundSchema;
