import { object, string, ObjectSchema } from 'yup';

export type AddQuestionRound = {
  question: string;
}

const AddQuestionRoundSchema: ObjectSchema<AddQuestionRound> = object({
  question: string().required(),
}).defined();

export default AddQuestionRoundSchema;
