import{ object, boolean, ObjectSchema } from "yup";

export type ValidateQuestion = {
  status: boolean;
}

const ValidateQuestionSchema: ObjectSchema<ValidateQuestion> = object({
  status: boolean().required(),
}).defined();

export default ValidateQuestionSchema;
