interface TeamScore {
  userId: string;
  points: number;
  name: string;
}

export default TeamScore;
