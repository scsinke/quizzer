import { Question } from "./question";
import { User } from "../schemas/userSchema";

interface Quiz {
  _id?: string;
  name: string;
  password?: string;
  language?: string;
  isFinished?: boolean;
  rounds?: Array<{
    _id?: string;
    categories: Array<string>;
    questions?: Array<{
      _id?: string;
      isFinished: boolean;
      question: Question;
      answers?: Array<{
        _id?: string;
        answer: string;
        status: boolean;
        user: User;
      }>;
    }>;
  }>;
}

export const questionsLimit = 12;
export const categorieLimit = 3;
export const minTeams = 2;
export const maxTeams = 6;

export default Quiz
