export const categories = [
  "Art and Literature",
  "General Knowledge",
  "Geography",
  "Music",
  "History",
  "Science and Nature",
  "Sport",
  "Film and TV",
  "Food and Drinks",
];

export const languages = [
  "en",
  "nl",
]

export interface Question {
  _id?: string;
  question: string;
  answer?: string;
  category: string;
}
