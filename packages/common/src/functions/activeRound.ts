import Quiz, { questionsLimit } from "../models/quiz";

export const getActiveRound = (quiz: Quiz) => quiz.rounds!.find(round => round.questions!.length < questionsLimit);
export const isActiveRound = (quiz: Quiz) => quiz.rounds!.some(round => round.questions!.length < questionsLimit);
