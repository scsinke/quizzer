import Quiz from "../models/quiz"

export const isActiveQuestion = (quiz: Quiz): boolean => {
  const isActive = quiz.rounds!.find(round => round.questions!.some(question => question.isFinished === false))
  if (!isActive) { return false }
  return true
}

export const getActiveQuestion = (quiz: any) =>
  quiz.rounds!
    .flatMap((round: any) => round.questions)
    .find((question: any) => question!.isFinished === false)
