import Quiz from "../models/quiz";

const getActiveQuiz = (quiz: Quiz) => quiz
  .rounds!.flatMap(round => round.questions)
  .find(question => question!.isFinished === false)

export default getActiveQuiz
