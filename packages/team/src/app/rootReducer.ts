import { combineReducers } from "@reduxjs/toolkit";
import quizzesSlice from "../components/quizzesList/quizzesSlice";
import quizSlice from "../components/quiz/quizSlice";
import { userSlice } from "@quizzer/ui";

const rootReducer = combineReducers({
  quizzesList: quizzesSlice,
  quiz: quizSlice,
  user: userSlice,
})

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
