import React from 'react';
import './App.css';

import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import QuizList from '../components/quizzesList/QuizList';
import QuizComponent from '../components/quiz/Quiz';
import { RootState } from './rootReducer';
import { connect } from 'react-redux';
import { UserState, Login, Register, UpdateUser } from '@quizzer/ui';
import Navbar from '../components/Navbar';
import JoinQuiz from '../components/JoinQuiz';

// if (process.env.NODE_ENV !== 'production') {
//   const {whyDidYouUpdate} = require('why-did-you-update');
//   whyDidYouUpdate(React);
// }

function App(props: UserState) {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route exact path="/quiz/join" component={JoinQuiz} />
          <Route path="/quiz/:quizId" component={QuizComponent} />
          <Route path="/quiz" component={QuizList} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/me" component={UpdateUser} />
          <Route path="/">
            <Redirect to={props.isLoggedIn ? '/quiz' : '/login'} />
          </Route>
        </Switch>
      </BrowserRouter>
    </div >
  );
}

const mapStateToProps = (state: RootState) => {
  return state.user;
}

export default connect(mapStateToProps)(App)
