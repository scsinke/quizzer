import React, { useEffect } from "react";
import { useDispatch, connect } from "react-redux";
import { getQuizzes, QuizzesState } from "./quizzesSlice";
import { Spin, Button } from "antd";
import { RootState } from "../../app/rootReducer";
import { NavLink, useHistory } from "react-router-dom";

const QuizList = ({ isLoading, quizzes }: QuizzesState) => {
  const dispatch = useDispatch()
  const history = useHistory();

  useEffect(() => {
    dispatch(getQuizzes())
  }, [dispatch])

  const returnValue = isLoading
    ? (<Spin />)
    : quizzes?.map(quiz => {

      if (quiz.isOwner) { return }

      return <NavLink to={`/quiz/${quiz.quiz._id}`} key={quiz.quiz._id}>
        <p>
          {quiz.quiz.name}
        </p>
      </NavLink>
    })

  const joinQuiz = () => {
    history.push('/quiz/join')
  }

  return (
    <div>
      <Button onClick={joinQuiz}>Join Quiz</Button>
      <h1>QuizList</h1>
      {returnValue}
    </div>
  )
}

const mapStateToProps = (state: RootState) => {
  return state.quizzesList;
}

export default connect(mapStateToProps)(QuizList)
