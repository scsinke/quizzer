import React, { FC } from "react";
import { Formik } from "formik";
import { Quiz } from "@quizzer/common";
import { Form, Input, SubmitButton, ResetButton } from "formik-antd";
import { message } from "antd";
import { useHistory } from "react-router-dom";

const joinQuiz = async (schema: Quiz) => {
  const res = await fetch('/api/v1/quiz/join', {
    method: 'POST',
    credentials: 'same-origin',
    cache: "no-cache",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(schema),
  });

  const body = await res.json();

  if (!res.ok) {
    message.error(body.message);
    return;
  }

  return body;
}

const JoinQuiz: FC = () => {
  const history = useHistory();

  const submit = async (value: Quiz) => {
    const quiz = await joinQuiz(value)
    if (!quiz) { return }
    history.push(`/quiz/${quiz._id}`)
  }

  return (
    <Formik<Quiz>
      initialValues={{
        name: '',
      }}
      onSubmit={submit}
    >
      <Form>
        <Form.Item name='name' label='Quiz name'>
          <Input name='name' />
        </Form.Item>
        <Form.Item name='password' label='password'>
          <Input name='password' type='password' />
        </Form.Item>
        <SubmitButton>Submit</SubmitButton>
        <ResetButton>Reset</ResetButton>
      </Form>
    </Formik>
  )
}

export default JoinQuiz;
