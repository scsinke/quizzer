import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import { QuizState, getQuizById } from "./quizSlice";
import { Spin } from "antd";
import { useParams } from "react-router-dom";
import io from "socket.io-client";
import { RootState } from "../../app/rootReducer";
import { Quiz, questionsLimit } from "@quizzer/common";
import { ScoreBoard } from "@quizzer/ui";
import Answer from "./Answer";

const isQuestionActive = (quiz: Quiz) => {
  if (quiz.isFinished) return false;

  const activeQuestion = quiz.rounds?.flatMap(round => round.questions).find(question => question?.isFinished === false)
  console.warn(activeQuestion)
  if(activeQuestion) return true

  return false
}

const isRoundActive = (quiz: Quiz) => !quiz.rounds?.every(round => round.questions!.length >= questionsLimit);

const QuizComponent = React.memo(({ quiz }: QuizState) => {
  const { quizId } = useParams()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getQuizById(quizId))

    const socket = io()

    socket.on('connect', () => {
      socket.emit('quiz', quizId)
      socket.on('message', (message: string) => {
        if (message !== 'UPDATE') return
        dispatch(getQuizById(quizId))
      })
    })

    return function cleanup () {
      socket.close()
    }
  }, [dispatch, quizId])

  if (!quiz || quiz._id !== quizId) {
    return <Spin />
  } else if (quiz.isFinished) {
    return <ScoreBoard quizId={quiz._id!} />
  } else if (isQuestionActive(quiz)) {
    return <Answer />
  } else if (!quiz.isFinished && !isRoundActive(quiz) && quiz.rounds?.length) {
    return <ScoreBoard quizId={quiz._id!} />
  }

  return (
    <div>
      <p>Waiting for quizmaster</p>
      <Spin />
    </div>
  )

});

const mapStateToProps = (state: RootState) => {
  return state.quiz;
}

export default connect(mapStateToProps)(QuizComponent)
