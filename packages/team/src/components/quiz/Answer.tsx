import React, { memo } from "react";
import { Formik, FormikHelpers } from "formik";
import { AnswerQuestion } from "@quizzer/common";
import { Form, Input, SubmitButton, ResetButton } from "formik-antd";
import { QuizState } from "./quizSlice";
import { RootState } from "../../app/rootReducer";
import { connect } from "react-redux";
import { message, Progress } from "antd";


const submit = async (value: AnswerQuestion, quizId: string, helpers: FormikHelpers<AnswerQuestion>) => {
  const res = await answerQuestion(quizId, value)
  if (!res.ok) {
    message.error("something went wrong when submitting your answer")
  } else {
    message.success("Answer submitted")
  }
  helpers.setValues(value)
  helpers.setSubmitting(false)
}

const answerQuestion = async (quizId: string, schema: AnswerQuestion) => {
  return fetch(`/api/v1/quiz/${quizId}/round/question/answer`, {
    method: 'PUT',
    credentials: 'same-origin',
    cache: "no-cache",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(schema),
  });

}

const Answer = memo(({ quiz }: QuizState) => {
  const round = quiz?.rounds![quiz.rounds?.length! - 1]
  const progress = 100 / 12 * round!.questions!.length;

  const question = round?.questions![round.questions?.length! - 1]

  return (

    <div>
      <Progress type="circle" percent={progress} format={() => `${round!.questions!.length}/12`} />
      <p>
        {question?.question.question}
      </p>
      <Formik<AnswerQuestion>
        initialValues={{
          answer: '',
        }}
        onSubmit={(value, helpers) => submit(value, quiz?._id!, helpers)}
      >
        <Form>
          <Form.Item name='answer' label='answer'>
            <Input name='answer' />
          </Form.Item>
          <SubmitButton>Submit</SubmitButton>
          <ResetButton>Reset</ResetButton>
        </Form>
      </Formik>
    </div>

  )
});

const mapStateToProps = (state: RootState) => {
  return state.quiz;
}

export default connect(mapStateToProps)(Answer)
