import '@quizzer/ui/src/components/navbar/navbar.css'

import { RootState } from "../app/rootReducer";
import { connect } from "react-redux";
import { Navbar } from "@quizzer/ui";

const mapStateToProps = (state: RootState) => {
  return state.user;
}

export default connect(mapStateToProps)(Navbar)
