import { Component } from "react";
import { Formik, FormikHelpers } from "formik";
import React from "react";
import { QuizSchema, Quiz } from "@quizzer/common";
import { Form, Input, SubmitButton, ResetButton, Radio } from "formik-antd";
import { message } from "antd";
import { withRouter, RouteComponentProps } from "react-router-dom";

const createQuiz = async (schema: Quiz) => {
  const res = await fetch('/api/v1/quiz', {
    method: 'POST',
    credentials: 'same-origin',
    cache: "no-cache",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(schema),
  });

  const body = await res.json();

  if (!res.ok) {
    message.error(body.message)
    return
  }

  return body;
}

interface IProps extends RouteComponentProps<any> { }

class CreateQuizComponent extends Component<IProps> {

  async submit(values: Quiz, { setSubmitting }: FormikHelpers<Quiz>) {
    const quiz = await createQuiz(values);
    if (!quiz) {
      setSubmitting(false);
      return;
    }
    this.props.history.push(`/quiz/${quiz._id}`)
  }


  render() {
    return (
      <Formik<Quiz>
        initialValues={{
          name: '',
          language: 'en',
        }}
        validationSchema={QuizSchema}
        onSubmit={this.submit.bind(this)}
      >
        <Form>
          <Form.Item name='name' label='Quiz name'>
            <Input name='name' />
          </Form.Item>
          <Radio.Group name="language" size="large" defaultValue="en">
            <Radio.Button value="en">English</Radio.Button>
            <Radio.Button value="nl">Dutch</Radio.Button>
          </Radio.Group>
          <Form.Item name='password' label='password'>
            <Input name='password' type='password' />
          </Form.Item>
          <SubmitButton>Submit</SubmitButton>
          <ResetButton>Reset</ResetButton>
        </Form>
      </Formik>

    )
  }
}

export default withRouter(CreateQuizComponent);
