import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "../../app/store";
import { Quiz } from "@quizzer/common";
import { message } from "antd";
import merge from "lodash.merge";

export interface QuizState {
  quiz?: Quiz;
  isApprovingTeams: boolean;
}

const initialState: QuizState = {
  isApprovingTeams: true,
}

const quiz = createSlice({
  name: "quizzes",
  initialState: initialState,
  reducers: {
    fetchQuizSuccess: (state, { payload }: PayloadAction<Quiz>) => {
      state.quiz = merge(state.quiz, payload)
    },
    doneApprovingTeams: (state) => {
      state.isApprovingTeams = false;
    },
    resetQuizState: (state) => {
      state.isApprovingTeams = true
      state.quiz = undefined
    }
  },
})

export const { fetchQuizSuccess, doneApprovingTeams, resetQuizState } = quiz.actions

export default quiz.reducer;

const fetchQuiz = async (quizId: string) => {
  const res = await fetch(`/api/v1/quiz/${quizId}`, {
    credentials: 'same-origin',
    cache: "no-cache",
  });

  const body = await res.json();

  if (!res.ok) {
    throw Error(body.message)
  }

  return body
}

export const getQuizById = (quizId: string): AppThunk => async dispatch => {
  try {
    const quiz = await fetchQuiz(quizId);
    dispatch(fetchQuizSuccess(quiz))
  } catch (error) {
    console.error(error)
    message.error("something went wrong with retrieving the quiz")
  }
}
