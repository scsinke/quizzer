import { useState } from "react";
import { Formik, FormikHelpers } from "formik";
import React from "react";
import { categories, CreateRound, CreateRoundSchema, categorieLimit } from "@quizzer/common";
import { Form, SubmitButton, ResetButton, Select } from "formik-antd";
import { RootState } from "../../app/rootReducer";
import { connect } from "react-redux";
import { QuizState } from "./quizSlice";
import { message } from "antd";
import EndQuiz from './EndQuiz';

const { Option } = Select;

const createRound = async (schema: CreateRound, quizId: string) => {
  const res = await fetch(`/api/v1/quiz/${quizId}/round`, {
    method: 'POST',
    credentials: 'same-origin',
    cache: "no-cache",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(schema),
  });

  if (!res.ok) {
    throw Error(await res.json());
  }
}

const submit = (values: CreateRound, quizId: string, { setSubmitting }: FormikHelpers<CreateRound>) => {
  try {
    createRound(values, quizId);
  } catch (error) {
    message.error("something went wrong while creating the round")
    console.error(error)
  }
  setSubmitting(false);
}

const CreateRoundComponent = (props: QuizState) => {
  const [optionsSelected, setOptionsSelected] = useState<string[]>([])

  const children = categories.map(categorie => {
    return (
      <Option
        key={categorie}
        value={categorie}
        disabled={
          optionsSelected.length > (categorieLimit - 1)
            ? optionsSelected.includes(categorie)
              ? false
              : true
            : false
        }
      >
        {categorie}
      </Option>
    )
  })

  return (
    <div>
      <h1>Create Round</h1>
      <Formik<CreateRound>
        initialValues={
          {
            categories: []
          }
        }
        validationSchema={CreateRoundSchema}
        onSubmit={(values, helpers) => submit(values, props.quiz?._id!, helpers)}
      >
        <Form>
          <Select
            mode="multiple"
            name="categories"
            placeholder="Please select three categories"
            style={{ width: '100%' }}
            onChange={setOptionsSelected}
          >
            {children}
          </Select>
          <SubmitButton>Create Round</SubmitButton>
          <ResetButton>Reset</ResetButton>
        </Form>

      </Formik >
      <EndQuiz />
    </div>
  )
}

const mapStateToProps = (state: RootState) => {
  return state.quiz;
}

export default connect(mapStateToProps)(CreateRoundComponent)


// class CreateRoundComponent extends Component {


//   async createRound(schema: CreateRound) {
//     const res = await fetch('/api/v1/quiz', {
//       method: 'POST',
//       credentials: 'same-origin',
//       cache: "no-cache",
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify(schema),
//     });
//     return res.json();
//   }

//   render() {
//     const children = categories.map(categorie => {
//       return (
//         <Option
//           key={categorie}
//           value={categorie}
//           disabled={true}
//         >
//           {categorie}
//         </Option>
//       )
//     })

//     return (
//       <Formik<CreateRound>
//         initialValues={
//           {
//             categories: []
//           }
//         }
//         validationSchema={CreateRoundSchema}
//         onSubmit={this.submit}
//       >
//         <Form>
//           <Select
//             mode="multiple"
//             name="categories"
//             placeholder="Please select two categories"
//             style={{ width: '100%' }}
//           >
//             {children}
//           </Select>
//           <SubmitButton>Create Round</SubmitButton>
//           <ResetButton>Reset</ResetButton>
//         </Form>

//       </Formik >
//     )
//   }
// }

// export default CreateRoundComponent;
