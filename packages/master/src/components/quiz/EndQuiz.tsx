import React from "react";
import { Button, message } from "antd";
import { connect } from "react-redux";
import { RootState } from "../../app/rootReducer";
import { QuizState } from "./quizSlice";


const endQuiz = async (quizId: string) => {
  const res = await fetch(`/api/v1/quiz/${quizId}/end`, {
    method: 'POST',
    credentials: 'same-origin',
    cache: "no-cache",
  });

  if (!res.ok) {
    message.error("something went wrong when ending the quiz")
    console.error(res.statusText);
  }
}

const endQuizComponent = (props: QuizState) => <Button onClick={() => endQuiz(props.quiz?._id!)}>End quiz</Button>

const mapStateToProps = (state: RootState) => {
  return state.quiz;
}

export default connect(mapStateToProps)(endQuizComponent)
