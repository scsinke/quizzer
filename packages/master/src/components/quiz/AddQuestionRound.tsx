import { useState, useEffect } from "react";
import React from "react";
import { AddQuestionRound, Question } from "@quizzer/common";
import { Form, SubmitButton, Select } from "formik-antd";
import { Formik, FormikHelpers } from "formik";
import { message } from "antd";
import { connect } from "react-redux";
import { RootState } from "../../app/rootReducer";
import { QuizState } from "./quizSlice";

const { Option } = Select;

const fetchAvailableQuestions = async (quizId: string) => {
  const res = await fetch(`/api/v1/quiz/${quizId}/round/question`, {
    credentials: 'same-origin',
    cache: "no-cache",
  });

  if (res.ok) {
    return await res.json();
  } else {
    console.error(res.statusText);
  }
}

const addQuestion = async (quizId: string, schema: AddQuestionRound) => await fetch(`/api/v1/quiz/${quizId}/round/question/`, {
  method: 'POST',
  credentials: 'same-origin',
  cache: "no-cache",
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(schema),
});

const submit = async (value: AddQuestionRound, quizId: string, { setSubmitting }: FormikHelpers<AddQuestionRound>) => {
  const res = await addQuestion(quizId, value)
  if (!res.ok) {
    message.error("Something went wrong");
    setSubmitting(false);
  } else {
    message.success("Added question")
  }
}

const AddQuestionRoundComponent = (props: QuizState) => {
  const [state, setQuestions] = useState<{ questions: Question[]; isFetching: boolean; }>({ questions: [], isFetching: false })

  useEffect(() => {
    setQuestions({ questions: state.questions, isFetching: true })
    const fetchQuestions = async () => {
      const res = await fetchAvailableQuestions(props.quiz?._id!);
      setQuestions({ questions: res, isFetching: false });
    }
    fetchQuestions()
    // Not putting props.quiz and state.questions in depedency array. this will result in a loop.
    // eslint-disable-next-line
  }, [])

  return (
    <div>
      <Formik<AddQuestionRound>
        initialValues={{ question: "" }}
        onSubmit={(value, helpers) => submit(value, props.quiz?._id!, helpers)}
      >
        <Form>
          <Select
            name='question'
            // FEATURE: implement search for many questions
            // showSearch={true}
            style={{ width: '100%' }}
          >
            {
              state.questions.map(question => <Option key={question._id!} value={question._id!}>{question.question}</Option>)
            }
          </Select>
          <SubmitButton>Add Question</SubmitButton>
        </Form>
      </Formik>
    </div>
  )
}

const mapStateToProps = (state: RootState) => {
  return state.quiz;
}

export default connect(mapStateToProps)(AddQuestionRoundComponent)
