import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import { RootState } from "../../app/rootReducer";
import { QuizState, getQuizById, resetQuizState } from "./quizSlice";
import { Spin } from "antd";
import { useParams } from "react-router-dom";
import ApproveTeamsComponent from "./ApproveTeams";
import AddQuestionRoundComponent from "./AddQuestionRound";
import { questionsLimit } from "@quizzer/common";
import { ScoreBoard } from "@quizzer/ui";
import CreateRoundComponent from "./CreateRound";
import ApproveAnswersComponent from "./ApproveAnswers";
import io from "socket.io-client";

const Quiz = ({ quiz, isApprovingTeams }: QuizState) => {
  const { quizId } = useParams()
  const dispatch = useDispatch()


  useEffect(() => {
    dispatch(getQuizById(quizId))

    const socket = io()

    socket.on('connect', () => {
      socket.emit('quiz', quizId)
      socket.on('message', (message: string) => {
        if (message !== 'UPDATE') return
        dispatch(getQuizById(quizId))
      })
    })

    return function cleanup () {
      socket.close()
      dispatch(resetQuizState())
    }

  }, [dispatch, quizId])

  if (!quiz || quiz._id !== quizId) {
    return <Spin />
  } else if (quiz.isFinished) {
    return <ScoreBoard quizId={quiz._id!} />
  } else if (!quiz.rounds?.length && isApprovingTeams) {
    return <ApproveTeamsComponent />
  } else if (!quiz.rounds?.length && !isApprovingTeams) {
    return <CreateRoundComponent />
  } else if (quiz.rounds?.length) {
    const round = quiz!.rounds![quiz!.rounds!.length - 1];
    if (!round.questions?.length) {
      return <AddQuestionRoundComponent />
    } else if (round.questions.length === questionsLimit && round.questions[round.questions.length - 1].isFinished) {
      return <CreateRoundComponent />
    } else if (!round.questions[round.questions.length - 1].isFinished) {
      return <ApproveAnswersComponent />
    } else {
      return <AddQuestionRoundComponent />
    }
  }

  return <Spin />
}

const mapStateToProps = (state: RootState) => {
  return state.quiz;
}

export default connect(mapStateToProps)(Quiz)
