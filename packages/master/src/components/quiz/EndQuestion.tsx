import React from "react";
import { Button, message } from "antd";
import { connect } from "react-redux";
import { RootState } from "../../app/rootReducer";
import { QuizState } from "./quizSlice";


const endQuestion = async (quizId: string) => {
  const res = await fetch(`/api/v1/quiz/${quizId}/round/question/end`, {
    method: 'POST',
    credentials: 'same-origin',
    cache: "no-cache",
  });

  if (!res.ok) {
    message.error("something went wrong when ending the question")
    console.error(res.statusText);
  }
}

const EndQuestionComponent = (props: QuizState) => <Button onClick={() => endQuestion(props.quiz?._id!)}>End Question</Button>

const mapStateToProps = (state: RootState) => {
  return state.quiz;
}

export default connect(mapStateToProps)(EndQuestionComponent)
