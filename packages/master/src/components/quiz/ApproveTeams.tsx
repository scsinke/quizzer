import { useEffect, useState } from "react";
import React from "react";
import { UpdateTeamStatus } from "@quizzer/common";
import { Radio, message, Button } from "antd";
import { RadioChangeEvent } from "antd/lib/radio";
import { connect, useDispatch } from "react-redux";
import { RootState } from "../../app/rootReducer";
import { QuizState, doneApprovingTeams } from "./quizSlice";

const approveTeam = async (quizId: string, userId: string, schema: UpdateTeamStatus) => {
  const res = await fetch(`/api/v1/quiz/${quizId}/team/${userId}`, {
    method: 'PUT',
    credentials: 'same-origin',
    cache: "no-cache",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(schema),
  });

  if (!res.ok) {
    return await res.json();
  }
}

const fetchTeams = async (quizId: string) => {
  const res = await fetch(`/api/v1/quiz/${quizId}/teams`, {
    credentials: 'same-origin',
    cache: "no-cache",
  });

  const body = await res.json();
  return body;
}

interface CustomUser {
  _id: string;
  name: string;
  quizzes: Array<{
    quiz: string;
    isOwner: boolean;
    isAccepted: boolean;
  }>;
}

const ApproveTeamsComponent = (props: QuizState) => {
  const dispatch = useDispatch();
  const [teams, setTeams] = useState<CustomUser[]>([])

  // fetch teams when entering component for the first time
  useEffect(() => {
    (async function getTeams() {
      const res = await fetchTeams(props.quiz?._id!)
      setTeams(res);
    })();
  }, [props.quiz])

  // FIXME: there should be a better fix for this
  // This ensures that the component is rerendered when teams are updated
  useEffect(() => {}, [teams])

  const submit = async (event: RadioChangeEvent, userId: string) => {
    if (!props.quiz || !props.quiz._id) { return }
    const val = event.target.value ? "Approving" : "Disapproving"
    message.loading({content: `${val} team`, key: userId }, 0);
    await approveTeam(props.quiz._id, userId, { isAccepted: event.target.value })
    const updatedTeams = await fetchTeams(props.quiz._id)
    setTeams(updatedTeams)
    message.success({content: "Team updated", key: userId}, 1)
  }

  const createRound = () => {
    dispatch(doneApprovingTeams())
  }

  return (
    <div>
      <h1>Approve teams</h1>
      <ul>
        {teams.map(team => {
          return (
            <div key={team.name}>
              {team.name}
              <Radio.Group onChange={event => submit(event, team._id!)} value={team.quizzes[0].isAccepted}>
                <Radio.Button value={true}>Accepted</Radio.Button>
                <Radio.Button value={false}>Rejected</Radio.Button>
              </Radio.Group>
            </div>
          )
        })}
      </ul>
      <Button type="primary" onClick={createRound}>Create round</Button>
    </div>
  )
}

const mapStateToProps = (state: RootState) => {
  return state.quiz;
}

export default connect(mapStateToProps)(ApproveTeamsComponent)
