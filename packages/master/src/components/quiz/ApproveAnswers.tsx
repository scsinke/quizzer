import React from "react";
import { ValidateQuestion } from "@quizzer/common";
import { Radio, message, Progress } from "antd";
import { RootState } from "../../app/rootReducer";
import { connect } from "react-redux";
import { QuizState } from "./quizSlice";
import EndQuestionComponent from "./EndQuestion";

const updateAnswerStatus = async (quizId: string, answerId: string, schema: ValidateQuestion) => {
  const res = await fetch(`/api/v1/quiz/${quizId}/round/question/answer/${answerId}`, {
    method: 'PUT',
    credentials: 'same-origin',
    cache: "no-cache",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(schema),
  });

  if (!res.ok) {
    message.error("something went wrong while approving the answer")
    return;
  }
  message.success("anwer approved")
}

const ApproveAnswersComponent = (props: QuizState) => {

  const { quiz } = props;

  const round = quiz?.rounds![quiz.rounds!.length - 1];
  const progress = 100 / 12 * round!.questions!.length

  const question = round?.questions![round.questions!.length - 1];

  const answers = question!.answers!

  return (
    <div>
      <h1>Approve Answers</h1>
      <Progress type="circle" percent={progress} format={() => `${round!.questions!.length}/12`} />
      <p>
        {question?.question.question}
        <br />
        {question?.question.answer}
      </p>
      {
        answers.map(answer => {
          return (
            <div key={answer._id}>
              {/* {answer.user.name} */}
              {answer.answer}
              <Radio.Group
                onChange={event => updateAnswerStatus(props.quiz?._id!, answer._id!, { status: event.target.value })}
                value={answer.status}
              >
                <Radio.Button value={true}>Correct</Radio.Button>
                <Radio.Button value={false}>False</Radio.Button>
              </Radio.Group>
            </div>
          )
        })
      }
      <EndQuestionComponent />
    </div>
  )
}

const mapStateToProps = (state: RootState) => {
  return state.quiz;
}

export default connect(mapStateToProps)(ApproveAnswersComponent)
