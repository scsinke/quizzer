import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { UserQuizes } from "@quizzer/common/dist/schemas/userSchema";
import { AppThunk } from "../../app/store";

const fetchQuizzes = async () => {
  const res = await fetch('/api/v1/quiz', {
    credentials: 'same-origin',
    cache: "no-cache",
  });
  const body = await res.json();

  if (!res.ok) {
    throw Error(body.message)
  }

  return body.quizzes
}

export interface QuizzesState {
  quizzes: UserQuizes;
  isLoading: boolean;
}

const initialState: QuizzesState = {
  quizzes: [],
  isLoading: false,
}

const startLoading = (state: QuizzesState) => {
  state.isLoading = true;
}

const quizzesSuccess = (state: QuizzesState, { payload }: PayloadAction<UserQuizes>) => {
  state.quizzes = payload;
  state.isLoading = false;
}

const quizzes = createSlice({
  name: "quizzes",
  initialState: initialState,
  reducers: {
    getQuizzesStart: startLoading,
    getQuizzesSuccess: quizzesSuccess,
  },
})

export const getQuizzes = (): AppThunk => async dispatch => {
  try {
    dispatch(getQuizzesStart())
    const response = await fetchQuizzes();
    dispatch(getQuizzesSuccess(response))
  } catch (error) {

  }
}

export const { getQuizzesStart, getQuizzesSuccess } = quizzes.actions

export default quizzes.reducer;
