import React from 'react';
import './App.css';

import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import QuizList from '../components/quizzesList/QuizList';
import CreateQuizComponent from '../components/CreateQuiz';
import Quiz from '../components/quiz/Quiz';
import { Login, Register, UpdateUser, UserState } from '@quizzer/ui';
import { RootState } from './rootReducer';
import { connect } from 'react-redux';
import Navbar from '../components/Navbar';

function App(props: UserState) {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route path="/quiz/create" component={CreateQuizComponent} />
          <Route path="/quiz/:quizId" component={Quiz} />
          <Route path="/quiz" component={QuizList} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/me" component={UpdateUser} />
          <Route path="/">
            <Redirect to={props.isLoggedIn ? '/quiz' : '/login'} />
          </Route>
        </Switch>
      </BrowserRouter>
    </div >
  );
}

const mapStateToProps = (state: RootState) => {
  return state.user;
}

export default connect(mapStateToProps)(App)
