const promises = require('fs').promises
const mongoose = require('mongoose');

mongoose.connect("mongodb+srv://scsinke:MIgeomwMIF72lpWV@cluster0-l2xov.azure.mongodb.net/quizzer", { useNewUrlParser: true, useUnifiedTopology: true })


const schema = new mongoose.Schema({
  question: { type: String, required: true },
  answer: { type: String, required: true },
  language: {
    type: String,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
});

const questionModel = mongoose.model('question', schema);


promises.readFile("Questions.json", 'utf-8')
  .then(async data => {
    const questions = JSON.parse(data);

    const res = await questionModel.create(questions)
  })
  .catch(err => {
    console.error(err)
  })
  .finally(() => {
    mongoose.connection.close();
  })
