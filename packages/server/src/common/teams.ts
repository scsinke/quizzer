import UserModel from "../models/user";
import { minTeams, maxTeams } from "@quizzer/common";

export const getNumberOfTeams = (quizId: string) => UserModel
  .find({
    "quizzes.quiz": quizId,
    "quizzes.isOwner": false,
    "quizzes.isAccepted": true,
  })
  .countDocuments()
  .exec();

export const isEnoughTeams = async (quizId: string) => {
  const numberOfTeams = await getNumberOfTeams(quizId);
  console.log("number of teams", numberOfTeams)
  if (numberOfTeams >= minTeams && numberOfTeams <= maxTeams) { return true }
  return false;
}
