import HttpStatusCode from "./statuscodes";

export default class HttpException extends Error {
  statusCode: HttpStatusCode;
  message: string;
  error: string | null;

  constructor(statusCode: HttpStatusCode, message: string, error?: string) {
    super(message);

    this.statusCode = statusCode;
    this.message = message;
    this.error = error || null;
  }
}
