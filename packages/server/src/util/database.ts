export const getEnumFromString = <T>(type: T, str: string): T[keyof T] => {
  if (!Number.isNaN(+str)) {
    const enumString = Number(str);
    const casted = enumString as keyof T;
    return type[casted];
  }
  const casted = str as keyof T;
  return type[casted];
};

export const getStringFromEnum = (type: any, val: any): string => type[val];
