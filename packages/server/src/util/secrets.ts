import * as dotenv from "dotenv";
import { existsSync } from "fs";
import logger from "./logger";

if (existsSync(".env")) {
  logger.debug("Using .env file to supply config environment variables");
  dotenv.config({ path: ".env" });
}

export const Environment = process.env.NODE_ENV;

export const SessionSecret = process.env.SESSION_SECRET;
export const MongoUrl = process.env.MONGODB_URI;
export const port = parseInt(process.env.PORT, 10) || 3000;

if (!SessionSecret) {
  logger.error("No client secret. Set SESSION_SECRET environment variable.");
  process.exit(1);
} else if (!MongoUrl) {
  logger.error(
    "No mongo connection string. Set MONGODB_URI environment variable.",
  );
  process.exit(1);
}
