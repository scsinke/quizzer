export const last = (arr: any[]) => arr[arr.length - 1];
export const first = (arr: any[]) => arr[0];
