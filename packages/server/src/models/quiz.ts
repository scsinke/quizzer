/* eslint-disable import/no-cycle */
import {
  Schema, Types, Document, Model, model,
} from "mongoose";
import { hash, verify } from "argon2";
import { User } from "./user";
import { InterfaceQuestion } from "./question";
import { categories, questionsLimit, categorieLimit, languages } from "@quizzer/common";

const validateCategoriesLimit = (arr: Array<string>) => arr.length <= categorieLimit;

export const quizSchema = new Schema({
  name: { type: String, required: true, unique: true },
  password: { type: String },
  language: { type: String, required: true, enum: languages },
  isFinished: { type: Boolean, required: true, default: false },
  rounds: [{
    categories: {
      type: [String],
      enum: categories,
      required: true,
      validate: [validateCategoriesLimit, `categories exceed the limit of ${categorieLimit}`],
    },
    questions: [{
      isFinished: { type: Boolean, required: true, default: false },
      question: {
        type: Types.ObjectId,
        ref: "Question",
      },
      answers: [{
        answer: {
          type: String,
          required: true,
        },
        status: {
          type: Boolean,
          default: false,
          required: true,
        },
        user: {
          type: Types.ObjectId,
          ref: "Team",
        },
      }],
    }],
  }],
});

const validateQuestionsLimit = (arr: unknown[]) => arr.length <= questionsLimit;
quizSchema.path("rounds.questions").validate((questions: unknown[]) => validateQuestionsLimit(questions), `questions exceed the limit of ${questionsLimit}`);

quizSchema.pre("save", async function (this: InterfaceQuiz, next) {
  if (!this.isModified("password") || !this.password) next();

  this.password = await hash(this.password);

  next();
});

quizSchema.method("comparePassword", async function (this: InterfaceQuiz, password: string) {
  if (this.password === undefined) return true;
  return await verify(this.password, password);
});

export interface InterfaceQuiz extends Document {
  name: string;
  password?: string;
  language: string;
  isFinished: boolean;
  rounds?: Array<{
    id?: string;
    categories: Array<string>;
    questions?: Array<{
      isFinished: boolean;
      question: InterfaceQuestion["_id"];
      answers?: Array<{
        id?: string;
        answer: string;
        status: boolean;
        user: User["_id"];
      }>;
    }>;
  }>;
  comparePassword(password: string): Promise<boolean>;
}

export type InterfaceQuizModel = Model<InterfaceQuiz>;

const QuizModel = model<InterfaceQuiz, InterfaceQuizModel>("Quiz", quizSchema);

export default QuizModel;
