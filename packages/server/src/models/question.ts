import {
  Schema, Model, model, Document,
} from "mongoose";
import { categories, Question, languages } from "@quizzer/common";

const questionSchema = new Schema({
  question: { type: String, required: true },
  answer: { type: String, required: true },
  language: {
    type: String,
    enum: languages,
    required: true,
  },
  category: {
    type: String,
    enum: categories,
    required: true,
  },
});

export interface InterfaceQuestion extends Question, Document {
  _id: any;
}

export type InterfaceQuestionModel = Model<InterfaceQuestion>;

const QuestionModel = model<InterfaceQuestion, InterfaceQuestionModel>("Question", questionSchema);

export default QuestionModel;
