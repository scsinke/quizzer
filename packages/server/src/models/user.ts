/* eslint-disable import/no-cycle */
import {
  Schema, Model, model, Document, Types,
} from "mongoose";
import { hash, verify } from "argon2";
import { InterfaceQuiz } from "./quiz";

const userSchema = new Schema(
  {
    name: { type: String, required: true, unique: true },
    password: { type: String, required: true, minlength: 10 },
    quizzes: [{
      _id: false,
      quiz: {
        type: Types.ObjectId,
        ref: "Quiz",
        required: true,
      },
      isOwner: {
        type: Boolean,
        required: true,
      },
      isAccepted: {
        type: Boolean,
        required() { return this.isOwner !== true; },
      },
    }],
  },
);

userSchema.pre("save", async function (this: User, next) {
  if (!this.isModified("password")) next();
  this.password = await hash(this.password);
  next();
});

userSchema.method("comparePassword", async function (this: User, password: string): Promise<boolean> {
  return await verify(this.password, password);
});


export interface User extends Document {
  name: string;
  password: string;
  quizzes?: Array<{
    quiz: InterfaceQuiz["_id"];
    isOwner: boolean;
    isAccepted?: boolean;
  }>;
  comparePassword(password: string): Promise<boolean>;
}

export type InterfaceUserModel = Model<User>;

const UserModel = model<User, InterfaceUserModel>("User", userSchema);

export default UserModel;
