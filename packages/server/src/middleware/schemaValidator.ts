import { Request, Response, NextFunction } from "express";
import HttpStatusCode from "../common/statuscodes";
import { ObjectSchema } from "yup";

const validation = (schema: ObjectSchema) => (req: Request, res: Response, next: NextFunction) => {
  schema.validate(req.body, { abortEarly: false, stripUnknown: true })
  .then(() => {
    next();
    return;
  })
  .catch(err => {
    console.log(err)
    err.errors
    res.status(HttpStatusCode.BAD_REQUEST).send({ error: err.errors });
  })
};

export default validation;
