import { Request, Response, NextFunction } from "express";
import HttpException from "../common/http-exception";
import HttpStatusCode from "../common/statuscodes";
import UserModel from "../models/user";

export const isQuizOwner = async (req: Request, res: Response, next: NextFunction) => {
  const { quizId } = req.params;
  const { userId } = req.session;

  const isOwner = await UserModel
    .find({
      _id: userId,
      "quizzes.quiz": quizId,
      "quizzes.isOwner": true,
    })
    .countDocuments()
    .exec();

  if (isOwner) {
    next();
    return;
  }

  next(new HttpException(HttpStatusCode.FORBIDDEN, "You don't have permission for this quiz"));
};

export const isQuizTeam = async (req: Request, res: Response, next: NextFunction) => {
  const { quizId } = req.params;
  const { userId } = req.session;

  const isNotOwner = await UserModel
    .find({
      _id: userId,
      "quizzes.quiz": quizId,
      "quizzes.isOwner": false,
      "quizzes.isAccepted": true,
    })
    .countDocuments()
    .exec();

  if (isNotOwner) {
    next();
    return;
  }

  next(new HttpException(HttpStatusCode.FORBIDDEN, "You don't have permission for this quiz"));
};

export const isQuizRelated = async (req: Request, res: Response, next: NextFunction) => {
  const { quizId } = req.params;
  const { userId } = req.session;

  const isRelated = await UserModel
    .find({
      _id: userId,
      "quizzes.quiz": quizId,
    })
    .countDocuments()
    .exec();

  if (isRelated) {
    next();
    return;
  }

  next(new HttpException(HttpStatusCode.FORBIDDEN, "You don't have permission for this quiz"));
};
