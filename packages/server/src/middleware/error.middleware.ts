import { Request, Response, NextFunction } from "express";
import HttpException from "../common/http-exception";
import HttpStatusCode from "../common/statuscodes";
import logger from "../util/logger";

export const errorHandler = (
  err: HttpException,
  req: Request,
  res: Response,
  _next: NextFunction,
) => {
  const status = err.statusCode || HttpStatusCode.INTERNAL_SERVER_ERROR;
  const message = err.message || "It's not you. It's us. We are having some problems.";


  logger.error({ message, status });
  res.status(status).send({ message });
};
