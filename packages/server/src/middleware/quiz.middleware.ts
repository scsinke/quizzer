import { Request, Response, NextFunction } from "express";
import QuizModel from "../models/quiz";
import HttpException from "../common/http-exception";
import HttpStatusCode from "../common/statuscodes";

const isQuizNotFinished = async (req: Request, res: Response, next: NextFunction) => {
  const { quizId } = req.params;
  const quiz = await QuizModel.findById(quizId);

  if (quiz.isFinished) {
    next(new HttpException(HttpStatusCode.BAD_REQUEST, "Quiz already finished"));
    return;
  }

  req.quiz = quiz;
  next();
};

export default isQuizNotFinished;
