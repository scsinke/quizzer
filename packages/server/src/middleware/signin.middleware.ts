import { Request, Response, NextFunction } from "express"
import HttpException from "../common/http-exception";
import HttpStatusCode from "../common/statuscodes";

const isSignedIn = (req: Request, res: Response, next: NextFunction) => {
  const { userId } = req.session;

  if (userId) {
    next();
    return;
  }

  next(new HttpException(HttpStatusCode.FORBIDDEN, "Not authenticated"));
};

export default isSignedIn;
