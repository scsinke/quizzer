import { Router } from "express";
import register from "./register";
import validation from "../../middleware/schemaValidator";
import { UserSchema } from "@quizzer/common";

const registerRouter = (router: Router) => router.post(
  "/register",
  validation(UserSchema),
  register,
);

export default registerRouter;
