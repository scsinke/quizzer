import { NextFunction, Request, Response } from "express";
import HttpStatusCode from "../../common/statuscodes";
import UserModel from "../../models/user";

const register = async (req: Request, res: Response, next: NextFunction) => {
  const { name, password } = req.body;

  try {
    const user = await UserModel.create({ name, password });
    res.status(HttpStatusCode.CREATED).send(user);
  } catch (error) {
    next(error);
  }
};

export default register;
