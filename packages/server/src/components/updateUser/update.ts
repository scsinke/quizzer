import { Request, Response, NextFunction } from "express";
import UserModel from "../../models/user";
import HttpStatusCode from "../../common/statuscodes";

const updateUser = async (req: Request, res: Response, next: NextFunction) => {
  const { userId } = req.session;
  const { name, password } = req.body

  try {
    const user = await UserModel.findById(userId);

    if (name && typeof name !== 'undefined') {
      user.name = name;
    } if (password && typeof password !== 'undefined') {
      user.password = password;
    }

    await user.save();

    res.sendStatus(HttpStatusCode.NO_CONTENT)
  } catch (error) {
    next(error);
  }
};

export default updateUser;
