import { Router } from "express";
import updateUser from "./update";
import validation from "../../middleware/schemaValidator";
import { UserUpdateSchema } from "@quizzer/common";
import isSignedIn from "../../middleware/signin.middleware";

const updateUserRouter = (router: Router) => router.put(
  "/me",
  isSignedIn,
  validation(UserUpdateSchema),
  updateUser,
);

export default updateUserRouter;
