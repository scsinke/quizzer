import { Request, Response, NextFunction } from "express";
import { startSession } from "mongoose";
import QuizModel from "../../models/quiz";
import UserModel from "../../models/user";
import HttpStatusCode from "../../common/statuscodes";

const createQuiz = async (req: Request, res: Response, next: NextFunction) => {
  const { name, password, language } = req.body;
  const { userId } = req.session;

  const session = await startSession();
  session.startTransaction();

  try {
    const quizzes = await QuizModel.create([{ name, password, language, isFinished: false }], { session });

    const quiz = quizzes[0];

    await UserModel.findByIdAndUpdate(userId, {
      $push: {
        quizzes: {
          quiz,
          isOwner: true,
        },
      },
    }, { upsert: true, new: true, session }).exec();

    await session.commitTransaction();
    session.endSession();
    res.status(HttpStatusCode.CREATED).send(quiz);
  } catch (err) {
    await session.abortTransaction();
    session.endSession();
    next(err);
  }
};

export default createQuiz;
