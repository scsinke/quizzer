import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import validation from "../../middleware/schemaValidator";
import createQuiz from "./createQuiz";
import { QuizSchema } from "@quizzer/common";

const createQuizRouter = (router: Router) => router.post(
  "/quiz",
  isSignedIn,
  validation(QuizSchema),
  createQuiz,
);

export default createQuizRouter;
