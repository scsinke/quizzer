import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import validation from "../../middleware/schemaValidator";
import { AddQuestionRoundSchema } from "@quizzer/common";
import addQuestionRound from "./addQuestionRound";
import { isQuizOwner } from "../../middleware/role.middleware";
import isQuizNotFinished from "../../middleware/quiz.middleware";

const addQuestionRoundRouter = (router: Router) => router.post(
  "/quiz/:quizId/round/question",
  isSignedIn,
  isQuizOwner,
  validation(AddQuestionRoundSchema),
  isQuizNotFinished,
  addQuestionRound,
);

export default addQuestionRoundRouter;
