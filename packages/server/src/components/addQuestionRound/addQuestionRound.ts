import { Request, Response, NextFunction } from "express";
import { InterfaceQuiz } from "../../models/quiz";
import HttpStatusCode from "../../common/statuscodes";
import HttpException from "../../common/http-exception";
import QuestionModel from "../../models/question";
import { isActiveQuestion, getActiveRound } from "@quizzer/common";

const addQuestionRound = async (req: Request, res: Response, next: NextFunction) => {
  const { quiz } = req;
  const { question } = req.body;

  if (isActiveQuestion(quiz)) {
    next(new HttpException(HttpStatusCode.BAD_REQUEST, "Still a question in progress"));
    return;
  }

  const activeRound = getActiveRound(quiz);
  if (!activeRound) {
    next(new HttpException(HttpStatusCode.BAD_REQUEST, "No active rounds"));
    return;
  }

  try {
    isQuestionAvailable(quiz, question);

    const { questions } = activeRound;

    questions.push({ isFinished: false, question });

    await quiz.save();
    res.sendStatus(HttpStatusCode.NO_CONTENT);
  } catch (error) {
    next(error);
  }
};

const isQuestionAvailable = async (quiz: InterfaceQuiz, questionReq: string) => {
  const questions = getUsedQuestions(quiz);
  const availableQuestions = await stillAvailableQuestions(questions, getActiveRound(quiz).categories)
  const availableQuestion = availableQuestions.find(question => question.id === questionReq);
  if (!availableQuestion) {
    throw new HttpException(HttpStatusCode.BAD_REQUEST, "The question is already used or belongs to a different category");
  }
}

const getUsedQuestions = (quiz: InterfaceQuiz) =>
  quiz.rounds
    .flatMap(round => round.questions
      .map(question => question.question)
    );

const stillAvailableQuestions = async (questions: any[], categories: string[]) => QuestionModel
  .find({
    _id: { $nin: questions },
    category: { $in: categories }
  })
  .exec();

export default addQuestionRound;
