import { Router } from "express";
import { ValidateQuestionSchema } from "@quizzer/common";
import isSignedIn from "../../middleware/signin.middleware";
import validation from "../../middleware/schemaValidator";
import validateQuestion from "./validateQuestion";
import { isQuizOwner } from "../../middleware/role.middleware";
import isQuizNotFinished from "../../middleware/quiz.middleware";

const validateQuestionRouter = (router: Router) => router.put(
  "/quiz/:quizId/round/question/answer/:answerId",
  isSignedIn,
  isQuizOwner,
  validation(ValidateQuestionSchema),
  isQuizNotFinished,
  validateQuestion,
);

export default validateQuestionRouter;
