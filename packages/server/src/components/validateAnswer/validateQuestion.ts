import { Request, Response, NextFunction } from "express";
import HttpStatusCode from "../../common/statuscodes";
import HttpException from "../../common/http-exception";
import { getActiveQuestion } from "@quizzer/common";
import { InterfaceQuiz } from "../../models/quiz";

const validateQuestion = async (req: Request, res: Response, next: NextFunction) => {
  const { quiz } = req;
  const { answerId } = req.params;
  const { status } = req.body;

  try {
    const activeQuestion = activeQuestionFromQuiz(quiz);
    const answer = answerFromQuestion(activeQuestion, answerId);

    answer.status = status;
    await quiz.save();
    res.sendStatus(HttpStatusCode.NO_CONTENT);
  } catch (error) {
    next(error);
  }
}

const activeQuestionFromQuiz = (quiz: InterfaceQuiz) => {
  const activeQuestion = getActiveQuestion(quiz);
  if (!activeQuestion) {
    throw new HttpException(HttpStatusCode.BAD_REQUEST, "No active question");
  }
  return activeQuestion;
}

const answerFromQuestion = (question, answerId: string) => {
  const answer = question.answers.find(answer => answer.id === answerId);
  if (!answer) {
    throw new HttpException(HttpStatusCode.BAD_REQUEST, "answerId is not matching")
  }
  return answer;
}

export default validateQuestion;
