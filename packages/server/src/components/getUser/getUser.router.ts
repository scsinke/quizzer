import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import getUser from "./getUser";

const getUserRouter = (router: Router) => router.get(
  "/me",
  isSignedIn,
  getUser,
);

export default getUserRouter;
