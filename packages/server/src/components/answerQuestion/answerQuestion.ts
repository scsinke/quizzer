import { Request, Response, NextFunction } from "express";
import HttpStatusCode from "../../common/statuscodes";
import QuestionModel from "../../models/question";
import { getActiveQuestion } from "@quizzer/common";
import HttpException from "../../common/http-exception";

const answerQuestion = async (req: Request, res: Response, next: NextFunction) => {
  const { quiz } = req;
  const { userId } = req.session;
  const { answer } = req.body;

  const activeQuestion = getActiveQuestion(quiz);

  if (!activeQuestion) {
   next(
     new HttpException(HttpStatusCode.BAD_REQUEST, "No active question")
   )
   return;
  }

  try {
    const dbQuestion = await QuestionModel.findById(activeQuestion.question);

    const status = dbQuestion.answer.toLowerCase() === answer.toLowerCase();

    const currentAnswer = activeQuestion.answers.find(answer => {
      return answer.user.toString() === userId
    });

    if (currentAnswer) {
      currentAnswer.answer = answer;
      currentAnswer.status = status;
    } else {
      activeQuestion.answers.push({ answer, status, user: userId })
    }

    await quiz.save();

    res.sendStatus(HttpStatusCode.NO_CONTENT);
  } catch (error) {
    next(error);
  }
}

export default answerQuestion;
