import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import validation from "../../middleware/schemaValidator";
import { AnswerQuestionSchema } from "@quizzer/common";
import answerQuestion from "./answerQuestion";
import { isQuizTeam } from "../../middleware/role.middleware";
import isQuizNotFinished from "../../middleware/quiz.middleware";

const answerQuestionRouter = (router: Router) => router.put(
  "/quiz/:quizId/round/question/answer",
  isSignedIn,
  isQuizTeam,
  validation(AnswerQuestionSchema),
  isQuizNotFinished,
  answerQuestion,
);

export default answerQuestionRouter;
