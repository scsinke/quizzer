import { Request, Response, NextFunction } from "express";
import HttpStatusCode from "../../common/statuscodes";
import UserModel from "../../models/user";
import QuizModel from "../../models/quiz";

type userPoints = Array<{ userId: string; points: number; name?: string; }>;

const getQuizScore = async (req: Request, res: Response, _next: NextFunction) => {
  const { quizId } = req.params;

  const teamQuery = UserModel
    .find({
      "quizzes.quiz": quizId,
      "quizzes.isOwner": false,
      "quizzes.isAccepted": true,
    }, "-password -quizzes")
    .exec();

  const quizQuery = QuizModel.findById(quizId).exec()

  const [teams, quiz] = await Promise.all([teamQuery, quizQuery])

  const finalResult = quiz.rounds.reduce((acc: userPoints, { questions }) => {
    const roundResult = questions.reduce((acc: userPoints, question) => {
      const correctAnswers = question.answers.filter(answer => answer.status)
      correctAnswers.forEach(answer => {
        const userId = answer.user.toString()
        const user = acc.find(team => team.userId === userId)
        if (user) {
          user.points += 1;
        } else {
          acc.push({ userId: userId, points: 1 })
        }
      })
      return acc;
    }, []);

    roundResult.sort((a, b) => b.points - a.points)

    roundResult.forEach(({ userId }, index) => {
      const points = getRoundScore(index);
      const team = acc.find(team => team.userId === userId)
      if (team) {
        team.points += points;
      } else {
        acc.push({ userId, points })
      }
    })
    return acc;
  }, [])

  const finalScore = finalResult.map(teamResult => {
    const team = teams.find(team => team._id.toString() === teamResult.userId)
    teamResult.name = team.name;
    return teamResult
  })

  res.status(HttpStatusCode.OK).send(finalScore)
}

const getRoundScore = (index: number) => {
  switch (index) {
    case 0:
      return 4
    case 1:
      return 2
    case 2:
      return 1;
    default:
      return 0.1;
  }
}

export default getQuizScore;
