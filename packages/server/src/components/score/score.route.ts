import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import { isQuizRelated } from "../../middleware/role.middleware";
import getQuizScore from "./score";

const scoreRouter = (router: Router) => router.get(
  "/quiz/:quizId/score",
  isSignedIn,
  isQuizRelated,
  getQuizScore,
);

export default scoreRouter;
