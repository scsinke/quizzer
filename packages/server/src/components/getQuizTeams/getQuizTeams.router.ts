import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import { isQuizOwner } from "../../middleware/role.middleware";
import getQuizTeams from "./getQuizTeams";

const getQuizTeamsRouter = (router: Router) => router.get(
  "/quiz/:quizId/teams",
  isSignedIn,
  isQuizOwner,
  getQuizTeams
);

export default getQuizTeamsRouter;
