import { NextFunction, Request, Response } from "express";
import UserModel from "../../models/user";
import HttpStatusCode from "../../common/statuscodes";

const getQuizTeams = async (req: Request, res: Response, next: NextFunction) => {
  const { quizId } = req.params;

  try {
    const users = await UserModel
      .find({
        quizzes: {
          $elemMatch: {
            quiz: quizId,
            isOwner: {
              $exists: true,
              $eq: false
            }
          }
        }
      },
      {
        "name": 1,
        "quizzes.$": 1
      })
      .lean()
      .exec();

    res.status(HttpStatusCode.OK).send(users);
  } catch (error) {
    next(error);
  }
};

export default getQuizTeams;
