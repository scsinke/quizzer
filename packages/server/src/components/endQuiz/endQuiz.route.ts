import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import { isQuizOwner } from "../../middleware/role.middleware";
import isQuizNotFinished from "../../middleware/quiz.middleware";
import endQuiz from "./endQuiz";

const endQuizRouter = (router: Router) => router.post(
  "/quiz/:quizId/end",
  isSignedIn,
  isQuizOwner,
  isQuizNotFinished,
  endQuiz,
);

export default endQuizRouter;
