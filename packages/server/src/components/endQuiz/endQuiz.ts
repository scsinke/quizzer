import { Request, Response, NextFunction } from "express"
import QuizModel, { InterfaceQuiz } from "../../models/quiz"
import { questionsLimit } from "@quizzer/common";
import HttpStatusCode from "../../common/statuscodes";
import HttpException from "../../common/http-exception";

const endQuiz = async (req: Request, res: Response, next: NextFunction) => {
  const { quizId } = req.params

  try {
    const quiz = await QuizModel.findById(quizId);

    const activeQuestion = quiz.rounds.find(round => round.questions.some(question => question.isFinished === false));
    const activeRound = getActiveRound(quiz);

    if (!activeQuestion && !activeRound) {
      quiz.isFinished = true;
      await quiz.save();
      res.sendStatus(HttpStatusCode.NO_CONTENT);
    } else {
      next(new HttpException(HttpStatusCode.BAD_REQUEST, "quiz still in progress"))
    }
  } catch (error) {
    next(error)
  }
}

const getActiveRound = (quiz: InterfaceQuiz) => quiz.rounds.find(round => round.questions.length < questionsLimit);

export default endQuiz
