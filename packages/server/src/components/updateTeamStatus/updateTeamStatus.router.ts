import { Router } from "express";
import validation from "../../middleware/schemaValidator";
import { UpdateTeamStatusSchema } from "@quizzer/common";
import updateTeamStatus from "./updateTeamStatus";
import isSignedIn from "../../middleware/signin.middleware";
import { isQuizOwner } from "../../middleware/role.middleware";
import isQuizNotFinished from "../../middleware/quiz.middleware";

const updateTeamStatusRouter = (router: Router) => router.put(
  "/quiz/:quizId/team/:userId",
  isSignedIn,
  isQuizOwner,
  validation(UpdateTeamStatusSchema),
  isQuizNotFinished,
  updateTeamStatus,
);

export default updateTeamStatusRouter;
