import { NextFunction, Request, Response } from "express";
import HttpStatusCode from "../../common/statuscodes";
import UserModel from "../../models/user";
import HttpException from "../../common/http-exception";
import { getNumberOfTeams } from "../../common/teams";
import { maxTeams } from "@quizzer/common";

const updateTeamStatus = async (req: Request, res: Response, next: NextFunction) => {
  const { quiz } = req;
  const { userId } = req.params;
  const { isAccepted } = req.body;

  if (quiz.rounds.length) {
    next(new HttpException(HttpStatusCode.FORBIDDEN, "Quiz already started"));
    return;
  }

  if (isAccepted && await getNumberOfTeams(quiz.id) >= maxTeams) {
    next(new HttpException(HttpStatusCode.BAD_REQUEST, `Max number of teams is ${maxTeams}`))
    return;
  }

  try {

    await UserModel
      .updateOne({
        _id: userId,
        quizzes: {
          $elemMatch: {
            quiz: quiz,
            isOwner: {
              $exists: true,
              $eq: false,
            }
          }
        }
      }, {
        $set: {
          "quizzes.$.isAccepted": isAccepted
        }
      }).exec()

    res.sendStatus(HttpStatusCode.NO_CONTENT);
  } catch (error) {
    next(error);
  }
};

export default updateTeamStatus;
