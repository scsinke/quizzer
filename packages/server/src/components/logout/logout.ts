import { NextFunction, Request, Response } from "express";
import HttpStatusCode from "../../common/statuscodes";

const logout = async (req: Request, res: Response, next: NextFunction) => {
  res.clearCookie('connect.sid')
  req.session.destroy(err => {
    if (err) next(err);
    else res.status(HttpStatusCode.OK).end();
  });
};

export default logout;
