import { Router } from "express";
import register from "./logout";
import isSignedIn from "../../middleware/signin.middleware";

const logoutRouter = (router: Router) => router.post(
  "/logout",
  isSignedIn,
  register,
);

export default logoutRouter;
