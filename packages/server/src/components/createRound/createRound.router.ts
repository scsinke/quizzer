import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import validation from "../../middleware/schemaValidator";
import { CreateRoundSchema } from "@quizzer/common";
import createRound from "./createRound";
import { isQuizOwner } from "../../middleware/role.middleware";
import isQuizNotFinished from "../../middleware/quiz.middleware";

const createRoundRouter = (router: Router) => router.post(
  "/quiz/:quizId/round",
  isSignedIn,
  isQuizOwner,
  validation(CreateRoundSchema),
  isQuizNotFinished,
  createRound,
);

export default createRoundRouter;
