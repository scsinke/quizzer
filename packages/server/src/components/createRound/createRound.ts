import { Request, Response, NextFunction } from "express";
import { InterfaceQuiz } from "../../models/quiz";
import HttpStatusCode from "../../common/statuscodes";
import HttpException from "../../common/http-exception";
import QuestionModel from "../../models/question";
import { questionsLimit, isActiveQuestion, isActiveRound, maxTeams, minTeams } from "@quizzer/common";
import UserModel from "../../models/user";
import { isEnoughTeams } from "../../common/teams";

const createRound = async (req: Request, res: Response, next: NextFunction) => {
  const { quiz } = req;
  const { categories } = req.body;
  
  if(!isEnoughTeams(quiz.id)) {
    next(new HttpException(HttpStatusCode.BAD_REQUEST, "No the correct amount of teams"));
    return;
  }

  try {
    isActiveroundInQuiz(quiz);

    await stillAvailableQuestions(
      getUsedQuestions(quiz),
      categories,
    )

    await quiz.updateOne({
      $push: {
        rounds: {
          categories,
        },
      },
    }).exec();

    res.sendStatus(HttpStatusCode.NO_CONTENT);
  } catch (error) {
    next(error);
  }
};

const isActiveroundInQuiz = (quiz: InterfaceQuiz) => {
  if (!isActiveQuestion(quiz) || !isActiveRound(quiz)) { return }
  throw new HttpException(HttpStatusCode.BAD_REQUEST, "Their is already an active round")
}

const getUsedQuestions = (quiz: InterfaceQuiz) =>
  quiz.rounds.flatMap(round =>
    round.questions.map(question => question.question)
  );

const stillAvailableQuestions = async (questions: any[], categories: string[]) => {
  const availableQuestions = await QuestionModel
    .find({
      _id: { $nin: questions },
      category: { $in: categories }
    })
    .countDocuments()
    .exec();

  if (availableQuestions >= questionsLimit) { return }

  throw new HttpException(HttpStatusCode.BAD_REQUEST, "No available questions left for these categorie");
}

export default createRound;
