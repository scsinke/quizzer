import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import getQuizzes from "./getQuizzes";

const getQuizzesRouter = (router: Router) => router.get(
  "/quiz",
  isSignedIn,
  getQuizzes,
);

export default getQuizzesRouter;
