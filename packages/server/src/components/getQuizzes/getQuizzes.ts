import { Request, Response, NextFunction } from "express";
import UserModel from "../../models/user";
import HttpStatusCode from "../../common/statuscodes";

const getQuizzes = async (req: Request, res: Response, next: NextFunction) => {
  const { userId } = req.session;

  try {
    const user = await UserModel
      .findById(userId)
      .select("-password -_id")
      .populate("quizzes.quiz", "-password")
      .lean()
      .exec();

    res.status(HttpStatusCode.OK).send(user);
  } catch (error) {
    next(error);
  }
};

export default getQuizzes;
