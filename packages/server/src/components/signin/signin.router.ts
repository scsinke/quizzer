import { Router } from "express";
import signin from "./signin";
import validation from "../../middleware/schemaValidator";
import { UserSchema } from "@quizzer/common";

const signinRouter = (router: Router) => router.post(
  "/login",
  validation(UserSchema),
  signin,
);

export default signinRouter;
