import { NextFunction, Request, Response } from "express";
import logger from "../../util/logger";
import HttpStatusCode from "../../common/statuscodes";
import HttpException from "../../common/http-exception";
import UserModel from "../../models/user";

const signin = async (req: Request, res: Response, next: NextFunction) => {
  const { name, password } = req.body;

  try {
    const user = await UserModel.findOne({ name });

    if (user && await user.comparePassword(password)) {
      req.session.userId = user.id;
      res.sendStatus(HttpStatusCode.NO_CONTENT);
      return;
    }

    logger.warn(`Login failed with username: ${name}`);

    next(new HttpException(HttpStatusCode.BAD_REQUEST, "The name or password provided was not correct"));
  } catch (error) {
    next(error);
  }
};

export default signin;
