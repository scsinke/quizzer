import { Router } from "express";
import createQuizRouter from "./createQuiz/createQuiz.router";
import registerRouter from "./register/register.router";
import signinRouter from "./signin/signin.router";
import logoutRouter from "./logout/logout.router";
import updateUserRouter from "./updateUser/updateUser.router";
import getQuizzesRouter from "./getQuizzes/getQuizzes.router";
import getQuizRouter from "./getQuiz/getQuiz.router";
import getUserRouter from "./getUser/getUser.router";
import updateTeamStatusRouter from "./updateTeamStatus/updateTeamStatus.router";
import getQuizTeamsRouter from "./getQuizTeams/getQuizTeams.router";
import createRoundRouter from "./createRound/createRound.router";
import addQuestionRoundRouter from "./addQuestionRound/addQuestionRound.router";
import endQuestionRouter from "./endQuestion/endQuestion.router";
import answerQuestionRouter from "./answerQuestion/answerQuestion.route";
import getAvailableQuestionsRouter from "./getAvailableQuestions/getAvailableQuestions.router";
import joinQuizRouter from "./joinQuiz/joinQuiz.router";
import validateQuestionRouter from "./validateAnswer/validateQuestion.route";
import endQuizRouter from "./endQuiz/endQuiz.route";
import scoreRouter from "./score/score.route";

const router = Router();

registerRouter(router);
signinRouter(router);
logoutRouter(router);

getUserRouter(router);
updateUserRouter(router);

createQuizRouter(router);
getQuizzesRouter(router);
getQuizRouter(router);
updateTeamStatusRouter(router);
getQuizTeamsRouter(router);
createRoundRouter(router);
addQuestionRoundRouter(router);
endQuestionRouter(router);
answerQuestionRouter(router);
getAvailableQuestionsRouter(router);
joinQuizRouter(router);
validateQuestionRouter(router);
endQuizRouter(router);
scoreRouter(router);

export default router;
