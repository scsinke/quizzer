import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import endQuestion from "./endQuestion";
import { isQuizOwner } from "../../middleware/role.middleware";
import isQuizNotFinished from "../../middleware/quiz.middleware";

const endQuestionRouter = (router: Router) => router.post(
  "/quiz/:quizId/round/question/end",
  isSignedIn,
  isQuizOwner,
  isQuizNotFinished,
  endQuestion,
);

export default endQuestionRouter;
