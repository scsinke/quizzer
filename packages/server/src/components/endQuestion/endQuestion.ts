import { Request, Response, NextFunction } from "express";
import HttpStatusCode from "../../common/statuscodes";
import HttpException from "../../common/http-exception";
import { getActiveQuiz } from "@quizzer/common";

const endQuestion = async (req: Request, res: Response, next: NextFunction) => {
  const { quiz } = req;

  try {
    const activeQuestion = quiz.rounds.flatMap(round => round.questions).find(question => question.isFinished === false)

    const activeQuiz = getActiveQuiz(quiz);

    if (!activeQuestion) {
      next(new HttpException(HttpStatusCode.BAD_REQUEST, "No current question"));
      return;
    }

    activeQuestion.isFinished = true;

    await quiz.save();

    res.sendStatus(HttpStatusCode.NO_CONTENT);
  } catch (error) {
    next(error);
  }
};

export default endQuestion;
