import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import getQuiz from "./getQuiz";
import { isQuizRelated } from "../../middleware/role.middleware";

const getQuizRouter = (router: Router) => router.get(
  "/quiz/:quizId",
  isSignedIn,
  isQuizRelated,
  getQuiz,
);

export default getQuizRouter;
