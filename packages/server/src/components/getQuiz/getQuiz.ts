import { Request, Response, NextFunction } from "express";
import HttpStatusCode from "../../common/statuscodes";
import QuizModel from "../../models/quiz";

const getQuiz = async (req: Request, res: Response, next: NextFunction) => {
  const { quizId } = req.params;

  try {
    const quiz = await QuizModel
      .findById(quizId, "-password")
      .populate("rounds.questions.question")
      .lean()
      .exec();

    res.status(HttpStatusCode.OK).send(quiz);
  } catch (error) {
    next(error);
  }
};

export default getQuiz;
