import { Router } from "express";
import isSignedIn from "../../middleware/signin.middleware";
import { isQuizOwner } from "../../middleware/role.middleware";
import isQuizNotFinished from "../../middleware/quiz.middleware";
import getAvailableQuestions from "./getAvailableQuestions";

const getAvailableQuestionsRouter = (router: Router) => router.get(
  "/quiz/:quizId/round/question",
  isSignedIn,
  isQuizOwner,
  isQuizNotFinished,
  getAvailableQuestions,
);

export default getAvailableQuestionsRouter;
