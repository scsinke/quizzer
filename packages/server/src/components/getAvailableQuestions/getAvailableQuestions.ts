import { Request, Response, NextFunction } from "express";
import { InterfaceQuiz } from "../../models/quiz";
import HttpStatusCode from "../../common/statuscodes";
import QuestionModel from "../../models/question";
import HttpException from "../../common/http-exception";
import { questionsLimit } from "@quizzer/common";

const getAvailableQuestions = async (req: Request, res: Response, next: NextFunction) => {
  const { quiz } = req;

  try {
    const round = quiz.rounds.find(round => {
      if(!round.questions.length) return round;

      if(round.questions.some(question => !question.isFinished)) {
        next(new HttpException(HttpStatusCode.BAD_REQUEST, "Still a question in progress"));
        return;
      }

      if (round.questions.length === questionsLimit) { return }

      return round
    });

    if (!round) {
      next(new HttpException(HttpStatusCode.BAD_REQUEST, "No round"))
      return;
    }

    const questions = getUsedQuestions(quiz);
    const availableQuestions = await stillAvailableQuestions(questions, round.categories, quiz.language)
    res.status(HttpStatusCode.OK).send(availableQuestions);
  } catch (error) {
    next(error);
  }
};

const getUsedQuestions = (quiz: InterfaceQuiz) =>
  quiz.rounds.flatMap(round =>
    round.questions.map(question => question.question)
  );

const stillAvailableQuestions = async (questions: any[], categories: string[], language: string) => QuestionModel
  .find({
    _id: { $nin: questions },
    category: { $in: categories },
    language: language,
  }, "question category")
  .exec();

export default getAvailableQuestions;
