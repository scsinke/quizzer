import { Router } from "express";
import { QuizSchema } from "@quizzer/common";
import isSignedIn from "../../middleware/signin.middleware";
import validation from "../../middleware/schemaValidator";
import joinQuiz from "./joinQuiz";

const joinQuizRouter = (router: Router) => router.post(
  "/quiz/join",
  isSignedIn,
  validation(QuizSchema),
  joinQuiz,
);

export default joinQuizRouter;
