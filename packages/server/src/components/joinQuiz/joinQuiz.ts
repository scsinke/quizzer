import { Request, Response, NextFunction } from "express";
import QuizModel from "../../models/quiz";
import UserModel from "../../models/user";
import HttpStatusCode from "../../common/statuscodes";
import HttpException from "../../common/http-exception";

const joinQuiz = async (req: Request, res: Response, next: NextFunction) => {
  const { name, password } = req.body;
  const { userId } = req.session;

  try {
    const quiz = await QuizModel
      .findOne({ name }, "name password")
      .exec();

    if (quiz.isFinished === true) {
      next(new HttpException(HttpStatusCode.FORBIDDEN, "Quiz is already finished"));
      return;
    }

    if (quiz.rounds || quiz.rounds?.length) {
      next(new HttpException(HttpStatusCode.FORBIDDEN, "Quiz already started"));
      return;
    }

    if (await quiz.comparePassword(password)) {
      await UserModel.findByIdAndUpdate(userId, {
        $push: {
          quizzes: {
            quiz,
            isOwner: false,
            isAccepted: false,
          },
        },
      }, { upsert: true, new: true }).exec();
    } else {
      next(new HttpException(HttpStatusCode.FORBIDDEN, "Incorrect password"));
      return;
    }

    res.status(HttpStatusCode.OK).send(quiz);
  } catch (err) {
    next(err);
  }
};

export default joinQuiz;
