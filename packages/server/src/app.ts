import {
  ConnectionOptions, connect, connection,
} from "mongoose";
import nocache from "nocache";
import express from "express";
import session from "express-session";
import bodyParser from "body-parser";
import helmet from "helmet";
import cors from "cors";
import mongo from "connect-mongo";
import { MongoUrl, SessionSecret } from "./util/secrets";
import logger from "./util/logger";
import { errorHandler } from "./middleware/error.middleware";
import { notFoundHandler } from "./middleware/notFound.middleware";
import router from "./components/index.route";

const app = express();

const MongoStore = mongo(session);
const connectionOptions: ConnectionOptions = {
  useFindAndModify: false,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
};

connect(MongoUrl, connectionOptions).catch(err => {
  logger.error(
    "MongoDb connection error. Please make sure MongoDB is running.",
    err,
  );
  process.exit(1);
});

app.use(
  session({
    resave: false,
    saveUninitialized: false,
    secret: SessionSecret,
    cookie: {
      sameSite: 'strict',
      maxAge: 2592000000, //30 days
      httpOnly: false,
    },
    store: new MongoStore({
      autoReconnect: true,
      mongooseConnection: connection,
    }),
  }),
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(helmet());
app.use(nocache());
app.use(cors({
  origin: true,
  credentials: true,
}));
app.use("/api/v1", router);

app.use(notFoundHandler);
app.use(errorHandler);

export default app;
