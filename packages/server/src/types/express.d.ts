import { InterfaceQuiz } from "../models/quiz";

declare module 'express-serve-static-core' {
  interface Request {
    quiz?: InterfaceQuiz;
  }
  interface Response {}
}
