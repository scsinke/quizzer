import { connection } from "mongoose";
import { port } from "./util/secrets";
import logger from "./util/logger";
import { createServer } from "http";
import app from "./app";
import createSocketServer from "./socket";

const SERVER = createServer(app)

// Gracefully close webscoket connection
// const disconnectWebsocket = () => {
//   SOCKETSERVER.close();
//   Object.values(SOCKETSERVER.of("/").connected).forEach(socket => socket.disconnect(true))
// }

// Gracefully close Mongo connection
const gracefulExit = () => {
  // disconnectWebsocket()

  connection.close(false, () => {
    logger.info("Mongo closed");
    SERVER.close(() => {
      logger.info("shutting down...");
    })
  });
};

// Server start
SERVER.listen(port, "0.0.0.0", () => {
  logger.info(`Running on port:${port}`);

  // Handle kill commands
  process.on("SIGINT", gracefulExit);
  process.on("SIGTERM", gracefulExit);

  // Prevent dirty exit on code-fault crashes
  process.on('uncaughtException', gracefulExit);
});

createSocketServer(SERVER);

export default SERVER;
