import { Server } from "http";
import { listen, ServerOptions } from "socket.io";
import QuizModel from "./models/quiz";
import http from "http";

const options: ServerOptions = {
  serveClient: false,
  perMessageDeflate: false,
  cookie: false,
}

const createSocketServer = (server: Server) => {
  const socketServer = listen(server, options)

  socketServer.on('connection', socket => {
    socket.on('quiz', async (quizId: string) => {
      socket.leaveAll();
      const quiz = await QuizModel.findById(quizId).exec();
      if (!quiz.isFinished) {
        socket.join(quizId);
      }
    })
  })

  QuizModel.watch().on('change', change => {
    if (change.operationType === 'update') {
      const quizId = change.documentKey._id.toString()
      sendUpdate(quizId)
      if (change.updateDescription.updatedFields.isFinished) {
        disconnectAllFromQuiz(quizId)
      }
    }
  });

  const sendUpdate = (quiz: string) => {
    socketServer.to(quiz).send("UPDATE")
  }

  const disconnectAllFromQuiz = (quiz: string) => {
    Object.values(
      socketServer.of("/")
        .in(quiz)
        .connected
    ).forEach(socket => socket.disconnect(true))
  }

  return socketServer;
}


export default createSocketServer;
