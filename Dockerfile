FROM node:latest-lts AS quizzer-base
WORKDIR /app
COPY . .
RUN yarn ci=true
RUN yarn build --production

FROM node:latest-lts AS quizzer-server
EXPOSE 8080
WORKDIR /app


FROM nginx:stable-alpine as quizzer-master
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
